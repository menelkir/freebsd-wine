FreeBSD Wine Builder
====================

This is a bunch of scripts to make it easier to build Wine+Wow64 on FreeBSD from HEAD.
There's no guarantees that will work, it's more like a contraption than a solution.
Also, this is not automated in any way, it's just a matter of building wine+wow64 being less painful.

============
Requirements
============

- As usual, you'll need almost the same requirements of wine on ports. You'll probably need more.
- A 32bit FreeBSD chroot

=============
How it works?
=============

1. Read the scripts
2. Make sure you have all the dependencies installed in both chroot and host (is pretty much the same as the wine in ports)
3. Create a chroot with a 32bit environment (e.g. /compat/i386)
4. Clone this repository somewhere inside the chroot (e.g. /compat/i386/root/wine)
5. Run stage1-64bit.sh in your 64bit host
6. Run stage2-32bit.sh in your 32bit chroot
7. Run stage3-32bit.sh in your 32bit chroot
8. Enter wine32 folder and make install (wine32 SHOULD be installed first)
9. Enter wine64 folder and make install
10. You will have your wine+wow64 at /opt/wine

============
Troubleshoot
============

gmake just freeze in 32bit chroot
    Rebuild bison without NLS

================
More information
================

:Build instructions: https://wiki.winehq.org/Building_Wine#Shared_WoW64
:Wine Sources: https://dl.winehq.org/wine/source/
:Wine GIT Repo: git://source.winehq.org/git/wine.git

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
