#!/bin/sh
cd wine64
CC=gcc10 XORG=x11 ./configure --disable-kerberos --disable-tests --without-alsa \
	--without-capi --without-dbus --without-gettext --without-gettextpo \
	--without-gphoto --without-gsm --without-gstreamer --without-inotify \
	--without-krb5 --without-mingw --without-opencl --without-osmesa \
	--with-oss --without-pulse --without-sane --without-tiff --without-udev \
	--without-unwind --without-usb --without-freetype --with-x --enable-win64 \
	--libdir=/opt/wine/lib --prefix=/opt/wine
CC=gcc10 gmake
